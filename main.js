/*
Statusses- Events
0 establish legation
1 elevate to embassy
2 closure
3 reopen embassy
4 establish embassy
5 reopen legation
*/
/*
Count: Returns the number of objects in a json file.
obj: the JSON file.
Source: StackOverflow
*/
function count(obj) {
  var count = 0;
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      ++count;
    }
  }
  return count;
}
/*
Initialize the two maps on the webpage on load.
*/
function initAll() {
  initMap(0);
  initMap2(0);
}

/*
This function performes the actual timelapse.
  - timeLapseTimeStep: The number of years to skip every timestep during the timeLapse
  - timeLapseDelay: The delay (in ms) between steps in the timeLapse
  - modals: the various events to show under the timeLapse
*/
var timeLapseTimeStep = 10;
var timeLapseDelay = 500;
var modals = {
  1787: $("#item1"),
  1917: $("#item2"),
  1940: $("#item3"),
  1967: $("#item4")
};


function loop(start, end, initStart) {
  if (start < end) {
    setTimeout(function() {
      start += timeLapseTimeStep;
      initMap2(start);
      var modals = {
        1787: $("#item1"),
        1917: $("#item2"),
        1940: $("#item3"),
        1967: $("#item4")
      }
      for (var key in modals) {
        if (modals.hasOwnProperty(key)) {
          if (start > key && key + 4 > initStart) {
            modals[key].show();
          }
        }
      }
      loop(start, end, initStart);
    }, timeLapseDelay);
  }
}
/*
Converts the status number to text, to be displayed on either map as a tooltip.
*/
function statusNumber(num) {
  switch (num) {
    case 0:
      return "Opened Legation";
      break;
    case 1:
      return "Opened Embassy";
      break;
    case 2:
      return "Closed Embassy/Legation";
      break;
    case 3:
      return "Reopened Embassy";
      break;
    case 4:
      return "Established Embassy";
      break;
    case 5:
      return "Reopened Legation";
      break;
  }
}
/*
Initializes the first map to a given year. If this function is called with an
invalid year, no points will be shown, allowing you to initialize the empty map.

When a correct year is provided, this function will draw the map in the year.
*/
function initMap(year) {
  curr = year;
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address="; //Google maps base API call URL.
  var adr = "ottawa"; //Initial variable state, not important.
  key = "&key=AIzaSyBLJhQHcguQ-zG98Ai_J4HmSBIgzKse2vI"; //The Google Maps API Key

  var map = new google.maps.Map(document.getElementById("map"), {
    zoom: 1,
    center: {
      lat: 0,
      lng: 0
    },
  });

  var points = [];
  //Fetch json file from AWS. Public file, so accessible from everywhere.
  fetch('https://s3.us-east-2.amazonaws.com/data-impacthack/currentData.json').then(function(response) {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Request failed!');
  }).then(function(data) {
    //console.log(data);

    for (var i = 0; i < count(data); i++) {
      //console.log(data[i].country);
      //console.log(data[i].lat);
      if (data[i].year <= year) { //Push all the JSON information to the dictionary data
        points.push({
          country: data[i].country, //Name of the country
          year: data[i].year, //The year in which the event ocurred.
          status: data[i].status, //The particular event.
          lat: data[i].lat, //Latitude of the embassy
          lon: data[i].lon //Longitude of the embassy
        });
      }
    }
  }).then(function(res) {
    //console.log(points);
    for (var z = 0; z < points.length; z++) {
      //console.log(points[z]); //
      //Basic switch statement in if form. Change the icons.
      var p = "https://s3.us-east-2.amazonaws.com/data-impacthack/BLUE.png";
      for (var x = 0; x <= 230; x++) {
        if (year - x == points[z].year) {
          if (points[z].status == 0) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/BLUE.png";
          if (points[z].status == 1) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/GREEN.png";
          if (points[z].status == 2) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/RED.png";
          if (points[z].status == 3) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/ORANGE.png";
          if (points[z].status == 4) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/PINK.png";
          if (points[z].status == 5) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/YELLOW.png";
        }
      }
      //console.log(loc);
      var marker = new google.maps.Marker({
        position: {
          lat: points[z].lat,
          lng: points[z].lon
        },
        map: map,
        //animation: google.maps.Animation.DROP,
        icon: p,
        title: points[z].country + " - " + statusNumber(points[z].status)
      });
    }
  });
}
/*
  A true copy of the initMap1 modified for the second map. Ideally, this would
  be combined with initMap1, which would take an additional parameter - the map
  that will be modified.
*/
function initMap2(year) {
  curr = year;
  var url = "https://maps.googleapis.com/maps/api/geocode/json?address=";
  var adr = "ottawa";
  key = "&key=AIzaSyBLJhQHcguQ-zG98Ai_J4HmSBIgzKse2vI";
  var map = new google.maps.Map(document.getElementById("map2"), {
    zoom: 1,
    center: {
      lat: 0,
      lng: 0
    },
  });

  var points = [];

  fetch('https://s3.us-east-2.amazonaws.com/data-impacthack/currentData.json').then(function(response) {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Request failed!');
  }).then(function(data) {
    //console.log(data);

    for (var i = 0; i < count(data); i++) {
      //console.log(data[i].country);
      //console.log(data[i].lat);
      if (data[i].year <= year) {
        points.push({
          country: data[i].country,
          year: data[i].year,
          status: data[i].status,
          lat: data[i].lat,
          lon: data[i].lon
        });
      }
    }
  }).then(function(res) {
    //console.log(points);
    for (var z = 0; z < points.length; z++) {
      console.log(points[z]);
      var p = "https://s3.us-east-2.amazonaws.com/data-impacthack/BLUE.png";
      for (var x = 0; x <= 230; x++) {
        if (year - x == points[z].year) {
          if (points[z].status == 0) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/BLUE.png";
          if (points[z].status == 1) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/GREEN.png";
          if (points[z].status == 2) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/RED.png";
          if (points[z].status == 3) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/ORANGE.png";
          if (points[z].status == 4) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/PINK.png";
          if (points[z].status == 5) p = "https://s3.us-east-2.amazonaws.com/data-impacthack/YELLOW.png";
        }
      }
      var loc = {
        lat: points[z].lat,
        lon: points[z].lon
      };
      //console.log(loc);
      var marker = new google.maps.Marker({
        position: {
          lat: points[z].lat,
          lng: points[z].lon
        },
        map: map,
        //animation: google.maps.Animation.DROP,
        icon: p,
        title: points[z].country + " - " + statusNumber(points[z].status)
      });
    }
  });
}
$(document).ready(function() {
  $("#C").click(function() {
    $('html, body').animate({
      scrollTop: $("#wrapper2").offset().top
    }, 600);
  });
  $("#B").click(function() {
    $('html, body').animate({
      scrollTop: $("#wrapper1").offset().top
    }, 600);
  });
  $("select").formSelect();
  $('.carousel').carousel();
  $("#span1").hide();
  $("#span2").hide();
  $(".collection-item").hide();
  $(".collection").hide();
  $('.modal').modal();
  $("#submit").click(function() {
    var input = document.getElementById("email_inline").value;
    if (!validator(input)) {
      $("#span1").slideDown();
    } else {
      initMap(parseInt(input));
      $("#span1").slideUp();
    }
    $("#options").empty();
    createOptions(parseInt(input));
    $("#placeinfo").hide();
  });
  $("#submit2").click(function() {
    var input1 = document.getElementById("email_inline1").value;
    var input2 = document.getElementById("email_inline2").value;
    if (!validator2(input1, input2)) {
      $("#span2").slideDown();
    } else {
      $("#span2").slideUp();
      var start = parseInt(input1);
      var end = parseInt(input2);
      $(".collection").show();
      $(".collection-item").hide();
      var initStart = start;
      loop(start, end, start);
    }
  });
});



function validator(input) {
  if (parseInt(input) >= 1780 && parseInt(input) <= 2018) {
    return true;
  }
  return false;
}

function setInfo() {
  var selectedOption = document.getElementById("options");

  fetch('https://s3.us-east-2.amazonaws.com/data-impacthack/des.json').then(function(response) {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Request failed!');
  }).then(function(data) {
    var option = selectedOption.options[selectedOption.selectedIndex].text.toLowerCase().trim();
    //console.log(data);

    $("#placeinfo").slideUp('fast', function() {

      $("#placeinfo").slideDown();
      $("#placeinfo").text(data[option]);
    });
  })
}
/*
  Creates the menu for part 1 of the webpage, to show the countries on the map
  for the user to get more info about each country.
*/
function createOptions(num) {
  fetch('https://s3.us-east-2.amazonaws.com/data-impacthack/data.json').then(function(response) {
    if (response.ok) {
      return response.json();
    }
    throw new Error('Request failed!');
  }).then(function(data) {
    $("#options").empty();
    for (var i = 0; i <= 175; i++) {
      if (data[i].year <= num) {
        var select = document.getElementById("options");
        var option = document.createElement("option");
        option.text = data[i].country;
        select.add(option);
        $("#options").formSelect();
      }
    }
  });
}
/*
Validates the years.
*/
function validator2(input1, input2) {
  if (parseInt(input1) < 1780 || parseInt(input1) > 2018) {
    return false;
  }
  if (parseInt(input2) < 1780 || parseInt(input2) > 2018) {
    return false;
  }
  if (parseInt(input1) >= parseInt(input2)) {
    return false;
  }
  return true;
}